import java.util.*;

public class JualBeliSaham {
    public static void main(String args[]) {
        int[] intArray = new int[]{5, 6, 15, 3, 10, 22, 15};
        
        int result = 0;
        
        for(int i=0; i<intArray.length-1; i++)
        {
            for(int x=i+1; x<intArray.length; x++)
            {
                int temp = intArray[x] - intArray[i];
                if(temp > result) result = temp;
            }
        }
        
        if(result > 0)
        {
            System.out.println(result);
        }
        else
        {
            System.out.println("Harga selalu turun");
        }
    }
}
