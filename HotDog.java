import java.util.*;

public class HotDog {
    public static void main(String args[]) {
        String[] stringArray = new String[]{"hot", "dot", "dog", "lot", "log"};
        String input1 = "hot";
        String input2 = "dog";
        
        List<String> result = new ArrayList<String>();
        result.add(input1);
        
        boolean isFound = false;
        
        for(int i=0; i<stringArray.length; i++)
        {
            if(input1 != stringArray[i]) 
            {
                int difference = FindDifference(input1, stringArray[i]);
                if(difference == 1)
                {
                    result.add(stringArray[i]);
                    input1 = stringArray[i];
                    
                    if(FindDifference(input1, input2) == 1)
                    {
                        result.add(input2);
                        isFound = true;
                        break;
                    }
                }
            }
        }
        
        if(isFound)
        {
            System.out.println(result);
        }
        else
        {
            System.out.println("<no way>");
        }
    }
    
    public static int FindDifference(String input1, String input2)
    {
        int result = 0;
        for(int i=0; i<input1.length(); i++)
        {
            if(input1.charAt(i) != input2.charAt(i)) result++;
        }
        return result;
    }
}
