import java.util.*;
import java.lang.Math;

public class Palindrome {
    public static void main(String args[]) {
        int input = 1;
        int result = 0;
        
        int start = (int)Math.pow(10, input-1);
        int limit = (int)Math.pow(10, input);
        
        for(int i=start; i<limit; i++)
        {
            for(int x=start; x<limit; x++)
            {
                int temp = i*x;
                if(temp >= 10)
                {
                    if(IsPalindrome(String.valueOf(temp)))
                    {
                        if(temp > result) result = temp;
                    }
                }
                else
                {
                    if(temp>result) result = temp;
                }
            }
        }
        
        System.out.println(result);
    }
    
    public static boolean IsPalindrome(String input)
    {
        int i = 0, j = input.length() - 1; 
  
        while (i < j) { 
            if (input.charAt(i) != input.charAt(j)) 
                return false; 
  
            i++; 
            j--; 
        } 
  
        return true; 
    }
}
