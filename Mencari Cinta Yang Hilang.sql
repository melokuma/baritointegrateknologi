SELECT pen.Nama
FROM Penduduk pen
INNER JOIN Pasangan pas ON pen.ID = pas.PendudukID
INNER JOIN Perpisahan per ON pas.PasanganID = per.PasanganID
ORDER BY per.TanggalPisah
FETCH FIRST 100 ROWS WITH TIES;
