import java.util.*;

public class PerfectTwo {
    public static void main(String args[]) {
        int[] intArray = new int[]{2,7,11,15};
        int input = 9;
        
        List<Integer> result = new ArrayList<Integer>();
        
        boolean isFound = false;
        for(int i=0; i<intArray.length; i++)
        {
            int candidate1 = intArray[i];
            for(int x=i+1; x<intArray.length; x++)
            {
                int candidate2 = intArray[x];
                if(candidate1 + candidate2 == input)
                {
                    result.add(i);
                    result.add(x);
                    isFound = true;
                }
                
                if(isFound)break;
            }
            if(isFound)break;
        }
        
        if(result.size() > 0)
        {
            System.out.println(result);
        }
        else
        {
            System.out.println("<no way>");
        }
    }
}
