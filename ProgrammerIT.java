import java.util.*;

public class ProgrammerIT {
    public static void main(String args[]) {
        String[] stringArray = new String[]{"pro", "gram", "merit", "program", "it", "programmer"};
        String input = "programmerit";
        
        List<String> result = new ArrayList<String>();
        
        int searchingIndex = 0;
        int searchingCounter = 0;
        String temp = input;
        boolean isFound = false;
        List<String> tempResult = new ArrayList<String>();
        
        while(searchingCounter<stringArray.length && searchingIndex<stringArray.length)
        {
            if(temp.contains(stringArray[searchingIndex]))
            {
                temp = temp.replace(stringArray[searchingIndex], "");
                tempResult.add(stringArray[searchingIndex]);
                if(temp.equals(""))
                {
                    temp = input;
                    isFound = true;
                    
                    String implodeResult = String.join(", ", tempResult);
                    if(result.contains(implodeResult) == false)
                    {
                        result.add(implodeResult);
                    }
                    
                    tempResult.clear();
                    
                    searchingCounter++;
                    searchingIndex = searchingCounter;
                }
            }
            searchingIndex++;
        }
        
        if(isFound) System.out.println(result);
        else System.out.println("<no way>");
    }
}
