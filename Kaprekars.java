import java.util.*;

public class Kaprekars {
    public static void main(String args[]) {
        int input = 3124;
        
        int kaprekarsConst = 0;
        int tempProcess = 0;
        
        while(tempProcess != 6174)
        {
            if(tempProcess == 0) tempProcess = input;
            char tempArray[] = Integer.toString(tempProcess).toCharArray();
            
            Arrays.sort(tempArray);
            String input1 = String.valueOf(tempArray);
            
            StringBuilder sb = new StringBuilder(input1);
            String input2 = sb.reverse().toString();
            
            tempProcess = Integer.parseInt(input2) - Integer.parseInt(input1);
            
            kaprekarsConst++;
        }
        
        System.out.println("Kaprekars Const is: " + kaprekarsConst);
    }
}
