SELECT DISTINCT ps.NamaPahlawan, ps.TempatTinggal
FROM PahlawanSuper ps 
INNER JOIN Senjata s ON ps.ID = s.PahlawanSuperID
INNER JOIN JenisSenjata js ON s.JenisSenjataID = js.ID
WHERE js.JenisSenjata = ‘Kapak’;
